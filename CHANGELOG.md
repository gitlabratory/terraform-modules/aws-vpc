# [1.1.0](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/compare/1.0.2...1.1.0) (2023-10-29)


### Bug Fixes

* **ci:** remove project ref & add jobs ([503d8f9](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/503d8f9baa5d172d05fcc9f43336a0bad34aa4b3))
* **ci:** tf module creation ([f7f6628](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/f7f6628ef3a834c099c7a4b6d231fc800de3f0dd))
* **ci:** tf module creation ([3f02c08](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/3f02c086eb46f2d30360c6f2101584d7b9c222d7))
* **ci:** tf module creation ([18cd354](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/18cd3543b9f225f7324079a19fa5ebb2b6937001))


### Features

* add remaining modules ([fa12112](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/fa12112975938b2ec68dbc4f392a8e28bac71dc8))
* add remaining modules ([e71bb5d](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/e71bb5d454c0b2229f9e58c4edc1a5e405ab3318))
* test ([b78e25e](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/b78e25e4e0f3a6e643513252d1d87c35b2dd5052))

## [1.0.2](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/compare/1.0.1...1.0.2) (2023-09-26)


### Bug Fixes

* **ci:** tf module creation ([1344dec](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/1344dec9fdd2f30f53d5b45fc13f9439474e4a0d))

## [1.0.1](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/compare/1.0.0...1.0.1) (2023-09-25)


### Bug Fixes

* **ci:** adding terraform template && git push ([0e07717](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/0e077177a1c847960da9cd8168586fb24491f289))

# 1.0.0 (2023-09-25)


### Features

* create aws-vpc module ([20199d0](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/20199d0d9e5b5e95e050e05f14e5b4a89a992f0f))
* create aws-vpc module ([70da234](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/70da23489329fd316d49ed32be7318a113dac3fc))
* create aws-vpc module ([1e3eb5c](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/1e3eb5c68ef3f8517641f554c13b959dd0b06552))
* create aws-vpc module ([647dffa](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/647dffaf7d3026c5d4cc523e2bf3b7fc5868c7de))
* create aws-vpc module ([d97c7cf](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/d97c7cfbeeff77ca037870b80fc27a8c4b82c28a))
* create aws-vpc module ([5d869d2](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/5d869d2fefb066668f2ae1301ad7d4ad9d7de202))
* create aws-vpc module ([0ef4c54](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/0ef4c543f30d113419c11c85e71e4b721a3768e4))
* create aws-vpc module ([aafa8a8](https://gitlab.com/gitlabratory/terraform-modules/aws-vpc/commit/aafa8a89cca1de0346a3afc12fc526a1fbd31880))
