variable "vpc_id" {}

resource "aws_network_acl" "main" {
  vpc_id = var.vpc_id

  egress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  ingress {
    protocol   = "-1"
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = {
    Name = "main-acl"
  }
}

output "network_acl_id" {
  value = aws_network_acl.main.id
}
