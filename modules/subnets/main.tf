variable "vpc_id" {}
variable "public_subnet_count" { default = 3 }
variable "private_subnet_count" { default = 3 }

resource "aws_subnet" "public" {
  count                   = var.public_subnet_count
  vpc_id                  = var.vpc_id
  cidr_block              = element(["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"], count.index)
  map_public_ip_on_launch = true

  tags = {
    Name = "public-subnet-${count.index}"
  }
}

resource "aws_subnet" "private" {
  count                   = var.private_subnet_count
  vpc_id                  = var.vpc_id
  cidr_block              = element(["10.0.4.0/24", "10.0.5.0/24", "10.0.6.0/24"], count.index)
  map_public_ip_on_launch = false

  tags = {
    Name = "private-subnet-${count.index}"
  }
}

output "public_subnet_ids" {
  value = aws_subnet.public[*].id
}

output "private_subnet_ids" {
  value = aws_subnet.private[*].id
}

output "public_subnet_cidr_blocks" {
  value = aws_subnet.public[*].cidr_block
}

output "private_subnet_cidr_blocks" {
  value = aws_subnet.private[*].cidr_block
}

output "public_subnet_availability_zones" {
  value = aws_subnet.public[*].availability_zone
}

output "private_subnet_availability_zones" {
  value = aws_subnet.private[*].availability_zone
}
