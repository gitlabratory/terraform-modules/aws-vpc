variable "public_subnet_id" {}

resource "aws_eip" "main" {
  vpc = true
}

resource "aws_nat_gateway" "main" {
  subnet_id = var.public_subnet_id
  allocation_id = aws_eip.main.id

  tags = {
    Name = "main-nat-gateway"
  }
}

output "nat_gateway_id" {
  value = aws_nat_gateway.main.id
}
