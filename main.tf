provider "aws" {
  region = var.aws_region
}

resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  tags = {
    Name = "main-vpc"
  }
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id
}

module "public_subnets" {
  source   = "./modules/subnets"
  vpc_id   = aws_vpc.main.id
  subnet_count = var.public_subnet_count
  subnet_type = "public"
}

module "private_subnets" {
  source   = "./modules/subnets"
  vpc_id   = aws_vpc.main.id
  subnet_count = var.private_subnet_count
  subnet_type = "private"
}
